﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using System.Collections.ObjectModel;



namespace CS481HW4
{
   

    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            Recipes();
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            Recipes();
            ListViewImages.IsRefreshing = false;
        }


        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            imageCellitem itemTapped = (imageCellitem)listView.SelectedItem;
            var uri = new Uri(itemTapped.url);
            Device.OpenUri(uri);

        }


        private void Recipes()
        {
            var imagesForListView = new ObservableCollection<imageCellitem>()
        {

            new imageCellitem ()
            {
                IconSource=ImageSource.FromFile("bakedLemonChicken.jpg"),
                Recipe="Baked Lemon Chicken",
                url= "https://www.allrecipes.com/recipe/8714/baked-lemon-chicken-with-mushroom-sauce/?internalSource=staff%20pick&referringId=664&referringContentType=Recipe%20Hub",
                Info="Ingredients and Directions",

            },

            new imageCellitem ()
            {
                IconSource=ImageSource.FromFile("applePie.jpg"),
                Recipe="Apple Pie",
                url= "https://www.allrecipes.com/recipe/20473/top-secret-apple-pie/?internalSource=staff%20pick&referringId=788&referringContentType=Recipe%20Hub",
                Info="Ingredients and Directions",

            },

              new imageCellitem ()
            {
                IconSource=ImageSource.FromFile("stripSteak.jpg"),
                Recipe="Strip Steak with Red Wine Cream Sauce",
                url= "https://www.allrecipes.com/recipe/231577/strip-steak-with-red-wine-cream-sauce/?internalSource=streams&referringId=1027&referringContentType=Recipe%20Hub&clickId=st_recipes_mades",
                Info="Ingredients and Directions",

            },

              new imageCellitem ()
            {
                IconSource=ImageSource.FromFile("cheesecake.jpg"),
                Recipe="Strawberry Cheesecake",
                url= "https://www.allrecipes.com/recipe/222588/strawberry-cheesecake/?internalSource=streams&referringId=387&referringContentType=Recipe%20Hub&clickId=st_recipes_mades",
                Info="Ingredients and Directions",

            },

              new imageCellitem ()
            {
                IconSource=ImageSource.FromFile("meatLasagna.jpg"),
                Recipe="Meat Lasagna",
                url= "https://www.allrecipes.com/recipes/16806/main-dish/pasta/lasagna/meat-lasagna/?internalSource=hub%20nav&referringId=502&referringContentType=Recipe%20Hub&linkName=hub%20nav%20daughter&clickId=hub%20nav%202",
                Info="Ingredients and Directions",

            },




        };

            ListViewImages.ItemsSource = imagesForListView;




        }

        void Handle_Delete(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var listItem = (ObservableCollection<imageCellitem>) ListViewImages.ItemsSource;

            imageCellitem list = (imageCellitem)menuItem.CommandParameter;
            listItem.Remove(list);
        }




    }



}